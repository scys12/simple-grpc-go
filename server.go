package main

import (
	"log"
	"net"

	"gitlab.com/scys12/simple-grpc-go/chat"
	"google.golang.org/grpc"
)

func main() {

	lis, err := net.Listen("tcp", ":9000")
	if err != nil {
		log.Fatalf("Failed to listen on port 9000")
	}

	cs := chat.Server{}

	grpcServer := grpc.NewServer()

	chat.RegisterChatServiceServer(grpcServer, &cs)

	if err := grpcServer.Serve(lis); err != nil {
		log.Fatalf("Failed to serve grpc Server over port 9000: %v", err)
	}
}
