module gitlab.com/scys12/simple-grpc-go

go 1.16

require (
	github.com/golang/protobuf v1.5.2 // indirect
	golang.org/x/net v0.0.0-20200822124328-c89045814202
	google.golang.org/grpc v1.41.0
	google.golang.org/protobuf v1.27.1 // indirect
)
